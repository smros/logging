import os
import logging
import logging.handlers
import socket
import sys
import time
class logger:
    def __init__(self, text_log_filename=None, network_text_log_dict=None, syslog_ip_tuple=None, email_log_dict=None, msg_header=None):
        
        self.text_log_filename=text_log_filename
        self.network_text_log_dict=network_text_log_dict
        self.syslog_ip_tuple=syslog_ip_tuple
        self.email_log_dict=email_log_dict
        self.msg_header=msg_header

        self.logger = logging.getLogger("")
        self.logger.setLevel(logging.DEBUG)

        self.formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        
        if self.text_log_filename!=None:
            # set up file handler
            file_hdl=logging.FileHandler(self.text_log_filename)
            file_hdl.setFormatter(self.formatter)
            self.logger.addHandler(file_hdl)

        if self.network_text_log_dict!=None:
            # set up file handler
            
            network_file_hdl=logging.FileHandler(self.network_text_log_dict['fname'])
            network_file_hdl.setFormatter(self.formatter)
            if self.network_text_log_dict['cache_memory']==True:
                memory_hdl=logging.handlers.MemoryHandler(capacity=2000, flushLevel=logging.ERROR, target=network_file_hdl)
                self.logger.addHandler(memory_hdl)

            else:
                self.logger.addHandler(network_file_hdl)
            
        if self.syslog_ip_tuple!=None:
            syslog_hdl=logging.handlers.SysLogHandler(address=self.syslog_ip_tuple)
            syslog_hdl.setFormatter(self.formatter)
            self.logger.addHandler(syslog_hdl)

        if self.email_log_dict !=None:
            # Add email handler here, note, check that the log level is set properly!
           
            email_hdl=logging.handlers.SMTPHandler(self.email_log_dict['server'],
                                                  self.email_log_dict['from_addr'],
                                                  self.email_log_dict['to_addr'],
                                                  self.email_log_dict['credentials'])
            email_hdl.setLevel(email_log_dict['logger_level'])
            email_hdl.setFormatter(self.formatter)
            self.logger.addHandler(email_hdl)
           

    def log_error(self, message):
        message = self.msg_header + ' , ' + message
        try:
            self.logger.error(message)
        except:
            raise 

    def log_debug(self, message):
        message = self.msg_header + ' , ' + message
        self.logger.debug(message)
        
    def log_info(self, message):
        message = self.msg_header + ' , ' + message
        self.logger.info(message)
                                                  

def logging_thread_server(log_queue, run_event,
                          text_log_filename=None,
                          network_text_log_dict=None,
                          syslog_ip_tuple=None,
                          email_log_dict=None,
                          msg_header=None,
                          loop_time=0.5
                          ):

    	
        logger_consumer=logger(text_log_filename,
                                        network_text_log_dict,
                                        syslog_ip_tuple,
                                        email_log_dict,
                                        msg_header)
                
                
	print 'Starting logging thread!'
	while run_event.is_set():
            if ~log_queue.isempty():
                # Messages should be dict of form (type: debug|info|error, message: text)
                try:
                    item = log_queue.pop()
                    item_type=item['type']
                    item_message=item['message']
                    if item_type=='debug':
                        logger_consumer.log_debug(item_message)
                    if item_type=='error':
                        logger_consumer.log_error(item_message)
                    else:
                    # assume any other type is info
                        logger_consumer.log_info(item_message)
                except:
                    print 'Error in logging thread'
                    # keep going, don't break execution in main thread...

            time.sleep(loop_time)
                    
                            

if __name__ == "__main__":
    message_header=socket.gethostname() + ', ' + os.getenv('username') + ', '
    test_fname='..\\logs\\test.log.txt'
    test_network_fname_dict={'fname':'..\\logs\\network_test.log.txt',
                             'log_level':None,
                             'cache_memory':True}
    test_syslog=('192.168.0.108',514)
    test_email_dict=None

    
    #test_email_dict={'server':('142.15.18.102',25),
    #                 'from_addr':'do-not-reply@calgarypolice.ca',
    #                 'to_addr':'c911@calgary.ca',
    #                 'credentials':None,
    #                 'logger_level':logging.ERROR
    #                 }
    #try:
    test=logger(test_fname, test_network_fname_dict, test_syslog, test_email_dict, message_header)

        
    test.log_debug('Debug test message')
    test.log_info('Info test message')
    test.log_error('ERROR Test message')

    #except:
    #    print 'Error in logging, attempting once more in INFO mode:'
    #    try:
    #        test.log_info('Error attempting to log')
    #    except:
    #        print 'Error on second try, abandoning..'
